package com.example.cafeterialorena.packageCafeteriaLorena;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.cafeterialorena.R;

public class MainActivity extends AppCompatActivity{

    int desayuno = 1;
    int panes = 1;
    int licuados = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void Desayuno (View view){
        Intent desayunoActividad = new Intent(this, Desayunos.class);
        desayunoActividad.putExtra("desayuno",desayuno);
        startActivity(desayunoActividad);
    }

    public void Panes (View view){
        Intent panesActividad = new Intent(this, Panes.class);
        panesActividad.putExtra("panes",panes);
        startActivity(panesActividad);
    }

    public void Licuados (View view){
        Intent licuadosActividad = new Intent(this, Licuados.class);
        licuadosActividad.putExtra("licuados",licuados);
        startActivity(licuadosActividad);
    }
}
