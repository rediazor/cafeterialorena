package com.example.cafeterialorena.packageCafeteriaLorena;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cafeterialorena.ApplicationCafeteriaLorena;
import com.example.cafeterialorena.R;

public class Panes extends AppCompatActivity implements View.OnClickListener{

    TextView txt_cantidadPanCarne, txt_cantidadPanPollo, txt_cantidadPanHuevoJamonQueso, txt_cantidadPanChorizoPita, txt_cantidadPanJamonQueso, txt_cantidadPanTomateCebolla, txt_cantidadPanHuevoSalchicha;
    static int cantidadPanCarne, cantidadPanPollo, cantidadPanHuevoJamonQueso, cantidadPanChorizoPita, cantidadPanJamonQueso, cantidadPanTomateCebolla, cantidadPanHuevoSalchicha;
    static String str_cantidadPanCarne, str_cantidadPanPollo, str_cantidadPanHuevoJamonQueso, str_cantidadPanChorizoPita, str_cantidadPanJamonQueso, str_cantidadPanTomateCebolla, str_cantidadPanHuevoSalchicha;

    TextView txt_precioPanCarne, txt_precioPanPollo, txt_precioPanHuevoJamonQueso, txt_precioPanChorizoPita, txt_precioPanJamonQueso, txt_precioPanTomateCebolla, txt_precioPanHuevoSalshicha;
    static int precioPanCarne, precioPanPollo, precioPanHuevoJamonQueso, precioPanChorizoPita, precioPanJamonQueso, precioPanTomateCebolla, precioPanHuevoSalchicha;
    static String str_precioPanCarne, str_precioPanPollo, str_precioPanHuevoJamonQueso, str_precioPanChorizoPita, str_precioPanJamonQueso, str_precioPanTomateCebolla, str_precioPanHuevoSalchicha;

    int detalle = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panes);

        // Intent Explicito
        int recibirPanes = getIntent().getExtras().getInt("panes");

        //
        //Del TextView a una variable entera para hacer calculos
        //
        txt_cantidadPanCarne = findViewById(R.id.txt_cantidadPanCarne);
        txt_cantidadPanPollo = findViewById(R.id.txt_cantidadPanPollo);
        txt_cantidadPanHuevoJamonQueso = findViewById(R.id.txt_cantidadPanHuevoJamonQueso);
        txt_cantidadPanChorizoPita = findViewById(R.id.txt_cantidadPanChorizoPita);
        txt_cantidadPanJamonQueso = findViewById(R.id.txt_cantidadPanJamonQueso);
        txt_cantidadPanTomateCebolla = findViewById(R.id.txt_cantidadPanTomateCebolla);
        txt_cantidadPanHuevoSalchicha = findViewById(R.id.txt_cantidadPanHuevoSalchicha);

        str_cantidadPanCarne = txt_cantidadPanCarne.getText().toString();
        str_cantidadPanPollo= txt_cantidadPanPollo.getText().toString();
        str_cantidadPanHuevoJamonQueso = txt_cantidadPanHuevoJamonQueso.getText().toString();
        str_cantidadPanChorizoPita = txt_cantidadPanChorizoPita.getText().toString();
        str_cantidadPanJamonQueso = txt_cantidadPanJamonQueso.getText().toString();
        str_cantidadPanTomateCebolla = txt_cantidadPanTomateCebolla.getText().toString();
        str_cantidadPanHuevoSalchicha = txt_cantidadPanHuevoSalchicha.getText().toString();

        cantidadPanCarne = Integer.parseInt(str_cantidadPanCarne);
        cantidadPanPollo = Integer.parseInt(str_cantidadPanPollo);
        cantidadPanHuevoJamonQueso = Integer.parseInt(str_cantidadPanHuevoJamonQueso);
        cantidadPanChorizoPita = Integer.parseInt(str_cantidadPanChorizoPita);
        cantidadPanJamonQueso = Integer.parseInt(str_cantidadPanJamonQueso);
        cantidadPanTomateCebolla = Integer.parseInt(str_cantidadPanTomateCebolla);
        cantidadPanHuevoSalchicha = Integer.parseInt(str_cantidadPanHuevoSalchicha);

        txt_precioPanCarne = findViewById(R.id.txt_precioPanCarne);
        txt_precioPanPollo = findViewById(R.id.txt_precioPanPollo);
        txt_precioPanHuevoJamonQueso = findViewById(R.id.txt_precioPanHuevoJamonQueso);
        txt_precioPanChorizoPita = findViewById(R.id.txt_precioPanChorizoPita);
        txt_precioPanJamonQueso = findViewById(R.id.txt_precioPanJamonQueso);
        txt_precioPanTomateCebolla = findViewById(R.id.txt_precioPanTomateCebolla);
        txt_precioPanHuevoSalshicha = findViewById(R.id.txt_precioPanHuevoSalchicha);

        str_precioPanCarne = txt_precioPanCarne.getText().toString();
        str_precioPanPollo = txt_precioPanPollo.getText().toString();
        str_precioPanHuevoJamonQueso = txt_precioPanHuevoJamonQueso.getText().toString();
        str_precioPanChorizoPita = txt_precioPanChorizoPita.getText().toString();
        str_precioPanJamonQueso = txt_precioPanJamonQueso.getText().toString();
        str_precioPanTomateCebolla = txt_precioPanTomateCebolla.getText().toString();
        str_precioPanHuevoSalchicha = txt_precioPanHuevoSalshicha.getText().toString();

        precioPanCarne = Integer.parseInt(str_precioPanCarne);
        ApplicationCafeteriaLorena.arrayPanes[0][0] = precioPanCarne;
        precioPanPollo = Integer.parseInt(str_precioPanPollo);
        ApplicationCafeteriaLorena.arrayPanes[1][0]=precioPanPollo;
        precioPanHuevoJamonQueso = Integer.parseInt(str_precioPanHuevoJamonQueso);
        ApplicationCafeteriaLorena.arrayPanes[2][0] = precioPanHuevoJamonQueso;
        precioPanChorizoPita = Integer.parseInt(str_precioPanChorizoPita);
        ApplicationCafeteriaLorena.arrayPanes[3][0] = precioPanChorizoPita;
        precioPanJamonQueso = Integer.parseInt(str_precioPanJamonQueso);
        ApplicationCafeteriaLorena.arrayPanes[4][0] = precioPanJamonQueso;
        precioPanTomateCebolla = Integer.parseInt(str_precioPanTomateCebolla);
        ApplicationCafeteriaLorena.arrayPanes[5][0] = precioPanTomateCebolla;
        precioPanHuevoSalchicha = Integer.parseInt(str_precioPanHuevoSalchicha);
        ApplicationCafeteriaLorena.arrayPanes[6][0] = precioPanHuevoSalchicha;

        //
        // Captura del boton del Layout
        //
        Button bt_panCarneMenos = (Button) findViewById(R.id.bt_panCarneMenos);
        Button bt_panCarneMas =  (Button) findViewById(R.id.bt_panCarneMas);
        Button bt_panPolloMenos = (Button) findViewById(R.id.bt_panPolloMenos);
        Button bt_panPolloMas = (Button) findViewById(R.id.bt_panPolloMas);
        Button bt_panHuevoJamonQuesoMenos = (Button) findViewById(R.id.bt_panHuevoJamonQuesoMenos);
        Button bt_panHuevoJamonQuesoMas = (Button) findViewById(R.id.bt_panHuevoJamonQuesoMas);
        Button bt_panChorizoPitaMenos = (Button) findViewById(R.id.bt_panChorizoPitaMenos);
        Button bt_panChorizoPitaMas = (Button) findViewById(R.id.bt_panChorizoPitaMas);
        Button bt_panJamonQuesoMenos = (Button) findViewById(R.id.bt_panJamonQuesoMenos);
        Button bt_panJamonQuesoMas = (Button) findViewById(R.id.bt_panJamonQuesoMas);
        Button bt_panTomateCebollaMenos = (Button) findViewById(R.id.bt_panTomateCebollaMenos);
        Button bt_panTomateCebollaMas = (Button) findViewById(R.id.bt_panTomateCebollaMas);
        Button bt_panHuevoSalchichaMenos = (Button) findViewById(R.id.bt_panHuevoSalchichaMenos);
        Button bt_panHuevoSalchichaMas = (Button) findViewById(R.id.bt_panHuevoSalchichaMas);
        Button bt_agregarOrdenPanes = (Button)findViewById(R.id.bt_agregarOrdenPanes);

        //
        // Registrar el onClickListener con la implementacion de arriba
        //
        bt_panCarneMenos.setOnClickListener(this);
        bt_panCarneMas.setOnClickListener(this);
        bt_panPolloMenos.setOnClickListener(this);
        bt_panPolloMas.setOnClickListener(this);
        bt_panHuevoJamonQuesoMenos.setOnClickListener(this);
        bt_panHuevoJamonQuesoMas.setOnClickListener(this);
        bt_panChorizoPitaMenos.setOnClickListener(this);
        bt_panChorizoPitaMas.setOnClickListener(this);
        bt_panJamonQuesoMenos.setOnClickListener(this);
        bt_panJamonQuesoMas.setOnClickListener(this);
        bt_panTomateCebollaMenos.setOnClickListener(this);
        bt_panTomateCebollaMas.setOnClickListener(this);
        bt_panHuevoSalchichaMenos.setOnClickListener(this);
        bt_panHuevoSalchichaMas.setOnClickListener(this);
        bt_agregarOrdenPanes.setOnClickListener(this);

        //inicializacion de array de panes
        // cada par de casillas representa un tipo de pan
        //arrayPanes = new int[6][2];
       /* for (int count = 0; count < 7; count++){
            for (int count1 = 0; count1 < 2; count1++){
                if (count1 == 0){
                    //inicializacion de casilla de precio igual a Q10
                    ApplicationCafeteriaLorena.arrayPanes[count][count1]= 10;
                } else {
                    //inicializacion casilla de cantidad igual a cero
                    ApplicationCafeteriaLorena.arrayPanes[count][count1]= 0;
                }
            }

        }*/
    }



      public void onClick(View v) {
          // do something when the button is clicked
          // Yes we will handle click here but which button clicked??? We don't know

          // So we will make
          switch (v.getId() /*to get clicked view id**/) {
              case R.id.bt_panCarneMenos:
                  cantidadPanCarne = restar(cantidadPanCarne);
                  txt_cantidadPanCarne.setText(cantidadPanCarne + "");
                  ApplicationCafeteriaLorena.arrayPanes[0][1] = cantidadPanCarne;
                  break;
              case R.id.bt_panCarneMas:
                  cantidadPanCarne = sumar(cantidadPanCarne);
                  txt_cantidadPanCarne.setText(cantidadPanCarne + "");
                  ApplicationCafeteriaLorena.arrayPanes[0][1] = cantidadPanCarne;
                  break;
              case R.id.bt_panPolloMenos:
                  cantidadPanPollo = restar(cantidadPanPollo);
                  txt_cantidadPanPollo.setText(cantidadPanPollo + "");
                  ApplicationCafeteriaLorena.arrayPanes[1][1] = cantidadPanPollo;
                  break;
              case R.id.bt_panPolloMas:
                  cantidadPanPollo = sumar(cantidadPanPollo);
                  txt_cantidadPanPollo.setText(cantidadPanPollo + "");
                  ApplicationCafeteriaLorena.arrayPanes[1][1] = cantidadPanPollo;
                  break;
              case R.id.bt_panHuevoJamonQuesoMenos:
                  cantidadPanHuevoJamonQueso = restar(cantidadPanHuevoJamonQueso);
                  txt_cantidadPanHuevoJamonQueso.setText(cantidadPanHuevoJamonQueso + "");
                  ApplicationCafeteriaLorena.arrayPanes[2][1] = cantidadPanHuevoJamonQueso;
                  break;
              case R.id.bt_panHuevoJamonQuesoMas:
                  cantidadPanHuevoJamonQueso = sumar(cantidadPanHuevoJamonQueso);
                  txt_cantidadPanHuevoJamonQueso.setText(cantidadPanHuevoJamonQueso + "");
                  ApplicationCafeteriaLorena.arrayPanes[2][1] = cantidadPanHuevoJamonQueso;
                  break;
              case R.id.bt_panChorizoPitaMenos:
                  cantidadPanChorizoPita = restar(cantidadPanChorizoPita );
                  txt_cantidadPanChorizoPita.setText(cantidadPanChorizoPita + "");
                  ApplicationCafeteriaLorena.arrayPanes[3][1] = cantidadPanChorizoPita;
                  break;
              case R.id.bt_panChorizoPitaMas:
                  cantidadPanChorizoPita = sumar(cantidadPanChorizoPita );
                  txt_cantidadPanChorizoPita.setText(cantidadPanChorizoPita + "");
                  ApplicationCafeteriaLorena.arrayPanes[3][1] = cantidadPanChorizoPita;
                  break;
              case R.id.bt_panJamonQuesoMenos:
                  cantidadPanJamonQueso = restar(cantidadPanJamonQueso );
                  txt_cantidadPanJamonQueso.setText(cantidadPanJamonQueso + "");
                  ApplicationCafeteriaLorena.arrayPanes[4][1] = cantidadPanJamonQueso;
                  break;
              case R.id.bt_panJamonQuesoMas:
                  cantidadPanJamonQueso = sumar(cantidadPanJamonQueso );
                  txt_cantidadPanJamonQueso.setText(cantidadPanJamonQueso + "");
                  ApplicationCafeteriaLorena.arrayPanes[4][1] = cantidadPanJamonQueso;
                  break;
              case R.id.bt_panTomateCebollaMenos:
                  cantidadPanTomateCebolla = restar(cantidadPanTomateCebolla);
                  txt_cantidadPanTomateCebolla.setText(cantidadPanTomateCebolla + "");
                  ApplicationCafeteriaLorena.arrayPanes[5][1] = cantidadPanTomateCebolla;
                  break;
              case R.id.bt_panTomateCebollaMas:
                  cantidadPanTomateCebolla = sumar(cantidadPanTomateCebolla);
                  txt_cantidadPanTomateCebolla.setText(cantidadPanTomateCebolla + "");
                  ApplicationCafeteriaLorena.arrayPanes[5][1] = cantidadPanTomateCebolla;
                  break;
              case R.id.bt_panHuevoSalchichaMenos:
                  cantidadPanHuevoSalchicha = restar(cantidadPanHuevoSalchicha);
                  txt_cantidadPanHuevoSalchicha.setText(cantidadPanHuevoSalchicha + "");
                  ApplicationCafeteriaLorena.arrayPanes[6][1] = cantidadPanHuevoSalchicha;
                  break;
              case R.id.bt_panHuevoSalchichaMas:
                  cantidadPanHuevoSalchicha = sumar(cantidadPanHuevoSalchicha);
                  txt_cantidadPanHuevoSalchicha.setText(cantidadPanHuevoSalchicha + "");
                  ApplicationCafeteriaLorena.arrayPanes[6][1] = cantidadPanHuevoSalchicha;
                  break;
              case R.id.bt_agregarOrdenPanes:
                  Intent detalleActividad = new Intent(this, DetalleOrden.class);
                  detalleActividad.putExtra("detalle",detalle);
                  startActivity(detalleActividad);
                  break;
          }
      }

    //
    /* Suma y resta cantidades**/
    //
    private int sumar(int vieneCantidad){
        if(vieneCantidad < 10)
            vieneCantidad++; // cantidad = cantidad + 1;
        else
            Toast.makeText(this, getString(R.string.str_msjCantidadMaxima), Toast.LENGTH_LONG).show();
        return (vieneCantidad);
    }

    private int restar(int vieneCantidad){
        if(vieneCantidad > 0)
            vieneCantidad--; // cantidad = cantidad - 1;
        else
            Toast.makeText(this, getString(R.string.str_msjCantidadCero), Toast.LENGTH_LONG).show();
        return (vieneCantidad);
    }
}
