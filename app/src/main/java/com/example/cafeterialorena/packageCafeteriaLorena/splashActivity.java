package com.example.cafeterialorena.packageCafeteriaLorena;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.cafeterialorena.R;
import java.util.Timer;
import java.util.TimerTask;

public class splashActivity extends AppCompatActivity {

    static final long SCREEN_DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        try {
            getSupportActionBar().hide(); //para qoculatar el action bar
        } catch (Exception e) {

        }

        //Intent utomatio
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Intent cambiarActividad = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(cambiarActividad);
                finish();
            }
        },SCREEN_DELAY);

    }
}
