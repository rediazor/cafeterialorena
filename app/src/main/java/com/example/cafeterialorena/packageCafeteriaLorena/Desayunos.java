package com.example.cafeterialorena.packageCafeteriaLorena;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cafeterialorena.ApplicationCafeteriaLorena;
import com.example.cafeterialorena.R;

public class Desayunos extends AppCompatActivity implements View.OnClickListener {

    int detalle = 1;

    TextView txt_cantidadEstrellados,
             txt_cantidadRevueltos,
             txt_cantidadTomateCebolla,
             txt_cantidadPita,
             txt_cantidadSalchicha,
             txt_cantidadJamon;

    String str_cantidadEstrellados,
            str_cantidadRevueltos,
            str_cantidadTomateCebolla,
            str_cantidadPita,
            str_cantidadSalchicha,
            str_cantidadJamon;

    int cantidadEstrellados,
        cantidadRevueltos,
        cantidadTomateCebolla,
        cantidadPita,
        cantidadSalchicha,
        cantidadJamon;

    /*Boolean bt_cafeEstrellados,
            bt_cafeRevueltos,
            bt_cafeTomateCebolla,
            bt_cafePita,
            bt_cafeSalchicha,
            bt_cafeJamon,
            bt_jugoEstrellados,
            bt_jugoRevueltos,
            bt_jugoTomateCebolla,
            bt_jugoPita,
            bt_jugoSalchicha,
            bt_jugoJamon;      */

    Button bt_estrelladosMenos,
           bt_revueltosMenos,
         bt_TomateCebollaMenos,
         bt_PitaMenos,
         bt_SalchichaMenos,
         bt_JamonMenos,
         bt_estrelladosMas,
         bt_revueltosMas,
         bt_TomateCebollaMas,
         bt_PitaMas,
         bt_SalchichaMas,
         bt_JamonMas,
         bt_jugoEstrellados,
         bt_cafeEstrellados,
         bt_jugoRevueltos,
         bt_cafeRevueltos,
         bt_jugoTomateCebolla,
         bt_cafeTomateCebolla,
         bt_jugoPita,
         bt_cafePita,
         bt_jugoSalchicha,
         bt_cafeSalchicha,
         bt_jugoJamon,
         bt_cafeJamon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_desayunos);


        int recibirDesayuno = getIntent().getExtras().getInt("desayuno");

        txt_cantidadEstrellados = findViewById(R.id.txt_cantidadExtrellados);
        txt_cantidadRevueltos = findViewById(R.id.txt_cantidadRevueltos);
        txt_cantidadTomateCebolla = findViewById(R.id.txt_cantidadTomateCebolla);
        txt_cantidadPita = findViewById(R.id.txt_cantidadPita);
        txt_cantidadSalchicha = findViewById(R.id.txt_cantidadSalchicha);
        txt_cantidadJamon = findViewById(R.id.txt_cantidadJamon);

        str_cantidadEstrellados = txt_cantidadEstrellados.getText().toString();
        str_cantidadRevueltos = txt_cantidadRevueltos.getText().toString();
        str_cantidadTomateCebolla = txt_cantidadTomateCebolla.getText().toString();
        str_cantidadPita = txt_cantidadPita.getText().toString();
        str_cantidadSalchicha = txt_cantidadSalchicha.getText().toString();
        str_cantidadJamon = txt_cantidadJamon.getText().toString();

        cantidadEstrellados = Integer.parseInt(str_cantidadEstrellados);
        cantidadRevueltos = Integer.parseInt(str_cantidadRevueltos);
        cantidadTomateCebolla= Integer.parseInt(str_cantidadTomateCebolla);
        cantidadPita= Integer.parseInt(str_cantidadPita);
        cantidadSalchicha= Integer.parseInt(str_cantidadSalchicha);
        cantidadJamon = Integer.parseInt(str_cantidadJamon);

        //
        // Captura del boton del Layout
        //
         bt_estrelladosMenos = (Button)findViewById(R.id.bt_estrelladosMenos);
         bt_revueltosMenos = (Button)findViewById(R.id.bt_revueltosMenos);
         bt_TomateCebollaMenos = (Button)findViewById(R.id.bt_TomateCebollaMenos);
         bt_PitaMenos          = (Button)findViewById(R.id.bt_PitaMenos);
         bt_SalchichaMenos     = (Button)findViewById(R.id.bt_SalchichaMenos);
         bt_JamonMenos         = (Button)findViewById(R.id.bt_JamonMenos);
         bt_estrelladosMas     = (Button)findViewById(R.id.bt_estrelladosMas);
         bt_revueltosMas       = (Button)findViewById(R.id.bt_revueltosMas);
         bt_TomateCebollaMas   = (Button)findViewById(R.id.bt_TomateCebollaMas);
         bt_PitaMas            = (Button)findViewById(R.id.bt_PitaMas);
         bt_SalchichaMas       = (Button)findViewById(R.id.bt_SalchichaMas);
         bt_JamonMas           = (Button)findViewById(R.id.bt_JamonMas);

         bt_jugoEstrellados    = (Button)findViewById(R.id.bt_jugoEstrellados);
         bt_cafeEstrellados    = (Button)findViewById(R.id.bt_cafeEstrellados);
         bt_jugoRevueltos      = (Button)findViewById(R.id.bt_jugoRevueltos);
         bt_cafeRevueltos      = (Button)findViewById(R.id.bt_cafeRevueltos);
         bt_jugoTomateCebolla  = (Button)findViewById(R.id.bt_jugoTomateCebolla);
         bt_cafeTomateCebolla  = (Button)findViewById(R.id.bt_cafeTomateCebolla);
         bt_jugoPita           = (Button)findViewById(R.id.bt_jugoPita);
         bt_cafePita           = (Button)findViewById(R.id.bt_cafePita);
         bt_jugoSalchicha      = (Button)findViewById(R.id.bt_jugoSalchicha);
         bt_cafeSalchicha      = (Button)findViewById(R.id.bt_cafeSalchicha);
         bt_jugoJamon          = (Button)findViewById(R.id.bt_jugoJamon);
         bt_cafeJamon          = (Button)findViewById(R.id.bt_cafeJamon);
        Button bt_agregarOrdenDesayunos = (Button)findViewById(R.id.bt_agregarOrdenDesayunos);

        bt_estrelladosMenos.setOnClickListener(this);
        bt_revueltosMenos.setOnClickListener(this);
        bt_TomateCebollaMenos.setOnClickListener(this);
        bt_PitaMenos.setOnClickListener(this);
        bt_SalchichaMenos.setOnClickListener(this);
        bt_JamonMenos.setOnClickListener(this);
        bt_estrelladosMas.setOnClickListener(this);
        bt_revueltosMas.setOnClickListener(this);
        bt_TomateCebollaMas.setOnClickListener(this);
        bt_PitaMas.setOnClickListener(this);
        bt_SalchichaMas.setOnClickListener(this);
        bt_JamonMas.setOnClickListener(this);
        bt_jugoEstrellados.setOnClickListener(this);
        bt_cafeEstrellados.setOnClickListener(this);
        bt_jugoRevueltos.setOnClickListener(this);
        bt_cafeRevueltos.setOnClickListener(this);
        bt_jugoTomateCebolla.setOnClickListener(this);
        bt_cafeTomateCebolla.setOnClickListener(this);
        bt_jugoPita.setOnClickListener(this);
        bt_cafePita.setOnClickListener(this);
        bt_jugoSalchicha.setOnClickListener(this);
        bt_cafeSalchicha.setOnClickListener(this);
        bt_jugoJamon.setOnClickListener(this);
        bt_cafeJamon.setOnClickListener(this);
        bt_agregarOrdenDesayunos.setOnClickListener(this);

    }
     /*Boolean
        cantidadEstrellados,
        cantidadRevueltos,
        cantidadTomateCebolla,
        cantidadPita,
        cantidadSalchicha,
        cantidadJamon;
      */
    public void onClick(View v) {
        switch (v.getId() /*to get clicked view id**/) {
            case R.id.bt_estrelladosMenos:
                cantidadEstrellados = restar(cantidadEstrellados);
                txt_cantidadEstrellados.setText(cantidadEstrellados + "");
                ApplicationCafeteriaLorena.arrayDesayunos[0][0] = cantidadEstrellados;
                break;
            case R.id.bt_revueltosMenos:
                cantidadRevueltos = restar(cantidadRevueltos);
                txt_cantidadRevueltos.setText(cantidadRevueltos + "");
                ApplicationCafeteriaLorena.arrayDesayunos[1][0] = cantidadRevueltos;
                break;
            case R.id.bt_TomateCebollaMenos:
                cantidadTomateCebolla = restar(cantidadTomateCebolla);
                txt_cantidadTomateCebolla.setText(cantidadTomateCebolla + "");
                ApplicationCafeteriaLorena.arrayDesayunos[2][0] = cantidadTomateCebolla;
                break;
            case R.id.bt_PitaMenos:
                cantidadPita = restar(cantidadPita);
                txt_cantidadPita.setText(cantidadPita + "");
                ApplicationCafeteriaLorena.arrayDesayunos[3][0] = cantidadPita;
                break;
            case R.id.bt_SalchichaMenos:
                cantidadSalchicha = restar(cantidadSalchicha);
                txt_cantidadSalchicha.setText(cantidadSalchicha + "");
                ApplicationCafeteriaLorena.arrayDesayunos[4][0] = cantidadSalchicha;
                break;
            case R.id.bt_JamonMenos:
                cantidadJamon = restar(cantidadJamon);
                txt_cantidadJamon.setText(cantidadJamon + "");
                ApplicationCafeteriaLorena.arrayDesayunos[5][0] = cantidadJamon;
                break;
            case R.id.bt_estrelladosMas:
                cantidadEstrellados = sumar(cantidadEstrellados);
                txt_cantidadEstrellados.setText(cantidadEstrellados + "");
                ApplicationCafeteriaLorena.arrayDesayunos[0][0] = cantidadEstrellados;
                break;
            case R.id.bt_revueltosMas:
                cantidadRevueltos = sumar(cantidadRevueltos);
                txt_cantidadRevueltos.setText(cantidadRevueltos + "");
                ApplicationCafeteriaLorena.arrayDesayunos[1][0] = cantidadRevueltos;
                break;
            case R.id.bt_TomateCebollaMas:
                cantidadTomateCebolla = sumar(cantidadTomateCebolla);
                txt_cantidadTomateCebolla.setText(cantidadTomateCebolla + "");
                ApplicationCafeteriaLorena.arrayDesayunos[2][0] = cantidadTomateCebolla;
                break;
            case R.id.bt_PitaMas:
                cantidadPita = sumar(cantidadPita);
                txt_cantidadPita.setText(cantidadPita + "");
                ApplicationCafeteriaLorena.arrayDesayunos[3][0] = cantidadPita;
                break;
            case R.id.bt_SalchichaMas:
                cantidadSalchicha = sumar(cantidadSalchicha);
                txt_cantidadSalchicha.setText(cantidadSalchicha + "");
                ApplicationCafeteriaLorena.arrayDesayunos[4][0] = cantidadSalchicha;
                break;
            case R.id.bt_JamonMas:
                cantidadJamon = sumar(cantidadJamon);
                txt_cantidadJamon.setText(cantidadJamon + "");
                ApplicationCafeteriaLorena.arrayDesayunos[5][0] = cantidadJamon;
                break;
            case R.id.bt_jugoEstrellados:
                ApplicationCafeteriaLorena.arrayDesayunos[0][1] = 1;
                break;
            case R.id.bt_cafeEstrellados:
                ApplicationCafeteriaLorena.arrayDesayunos[0][1] = 0;
                break;
            case R.id.bt_jugoRevueltos:
                ApplicationCafeteriaLorena.arrayDesayunos[1][1] = 1;
                break;
            case R.id.bt_cafeRevueltos:
                ApplicationCafeteriaLorena.arrayDesayunos[1][1] = 0;
                break;
            case R.id.bt_jugoTomateCebolla:
                ApplicationCafeteriaLorena.arrayDesayunos[2][1] = 1;
                break;
            case R.id.bt_cafeTomateCebolla:
                ApplicationCafeteriaLorena.arrayDesayunos[2][1] = 0;
                break;
            case R.id.bt_jugoPita:
                ApplicationCafeteriaLorena.arrayDesayunos[3][1] = 1;
                break;
            case R.id.bt_cafePita:
                ApplicationCafeteriaLorena.arrayDesayunos[3][1] = 0;
                break;
            case R.id.bt_jugoSalchicha:
                ApplicationCafeteriaLorena.arrayDesayunos[4][1] = 1;
                break;
            case R.id.bt_cafeSalchicha:
                ApplicationCafeteriaLorena.arrayDesayunos[4][1] = 0;
                break;
            case R.id.bt_jugoJamon:
                ApplicationCafeteriaLorena.arrayDesayunos[5][1] = 1;
                break;
            case R.id.bt_cafeJamon:
                ApplicationCafeteriaLorena.arrayDesayunos[5][1] = 1;
                break;
            case R.id.bt_agregarOrdenDesayunos:
                //public void detalle (View view){
                Intent detalleActividad = new Intent(this, DetalleOrden.class);
                detalleActividad.putExtra("detalle",detalle);
                startActivity(detalleActividad);
            //}
                break;
        }
    }

    //
    /* Suma y resta cantidades**/
    //
    private int sumar(int vieneCantidad){
        if(vieneCantidad < 10)
            vieneCantidad++; // cantidad = cantidad + 1;
        else
            Toast.makeText(this, getString(R.string.str_msjCantidadMaxima), Toast.LENGTH_LONG).show();
        return (vieneCantidad);
    }

    private int restar(int vieneCantidad) {
        if (vieneCantidad > 0)
            vieneCantidad--; // cantidad = cantidad - 1;
        else
            Toast.makeText(this, getString(R.string.str_msjCantidadCero), Toast.LENGTH_LONG).show();
        return (vieneCantidad);
    }
}
