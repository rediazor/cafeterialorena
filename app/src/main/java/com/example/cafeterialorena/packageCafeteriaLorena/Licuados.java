package com.example.cafeterialorena.packageCafeteriaLorena;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cafeterialorena.ApplicationCafeteriaLorena;
import com.example.cafeterialorena.R;

public class Licuados extends AppCompatActivity implements View.OnClickListener{

    TextView txt_CantidadLicuadoPapaya,
            txt_CantidadLicuadoMelon,
            txt_CantidadLicuadoPina,
            txt_CantidadLicuadoBanano,
            txt_CantidadLicuadoFresa;


    String str_CantidadLicuadoPapaya,
            str_CantidadLicuadoMelon,
            str_CantidadLicuadoPina,
            str_CantidadLicuadoBanano,
            str_CantidadLicuadoFresa;


    int CantidadLicuadoPapaya,
        CantidadLicuadoMelon,
        CantidadLicuadoPina,
        CantidadLicuadoBanano,
        CantidadLicuadoFresa;

    int detalle = 1;

    /*boolean bt_aguaLicuadoPapaya,
            bt_lecheLicuadoPapaya,
            bt_aguaLicuadoMelon,
            bt_lecheLicuadoMelon,
            bt_aguaLicuadoPina,
            bt_lecheLicuadoPina,
            bt_aguaLicuadoBanano,
            bt_lecheLicuadoBanano,
            bt_aguaLicuadoFresa,
            bt_lecheLicuadoFresa; */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_licuados);

        int recibirLicuados = getIntent().getExtras().getInt("licuados");

        txt_CantidadLicuadoPapaya = findViewById(R.id.txt_cantidadLicuadoPapaya);
        txt_CantidadLicuadoMelon = findViewById(R.id.txt_cantidadLicuadoMelon);
        txt_CantidadLicuadoPina = findViewById(R.id.txt_cantidadLicuadoPina);
        txt_CantidadLicuadoBanano = findViewById(R.id.txt_cantidadLicuadoBanano);
        txt_CantidadLicuadoFresa = findViewById(R.id.txt_cantidadLicuadoFresa);


        str_CantidadLicuadoPapaya =txt_CantidadLicuadoPapaya.getText().toString();
        str_CantidadLicuadoMelon =txt_CantidadLicuadoMelon.getText().toString();
        str_CantidadLicuadoPina =txt_CantidadLicuadoPina.getText().toString();
        str_CantidadLicuadoBanano =txt_CantidadLicuadoBanano.getText().toString();
        str_CantidadLicuadoFresa =txt_CantidadLicuadoFresa.getText().toString();


        CantidadLicuadoPapaya = Integer.parseInt(str_CantidadLicuadoPapaya);
        CantidadLicuadoMelon = Integer.parseInt(str_CantidadLicuadoMelon);
        CantidadLicuadoPina = Integer.parseInt(str_CantidadLicuadoPina);
        CantidadLicuadoBanano = Integer.parseInt(str_CantidadLicuadoBanano);
        CantidadLicuadoFresa = Integer.parseInt(str_CantidadLicuadoFresa);


        //
        // Captura del boton del Layout
        //
        Button bt_CantidadLicuadoPapayaMas = (Button) findViewById(R.id.bt_licuadoPapayaMas);
        Button bt_CantidadLicuadoPapayaMenos = (Button) findViewById(R.id.bt_licuadoPapayaMenos);
        Button bt_CantidadLicuadoMelonMas = (Button) findViewById(R.id.bt_licuadoMelonMas);
        Button bt_CantidadLicuadoMelonMenos = (Button) findViewById(R.id.bt_licuadoMelonMenos);
        Button bt_CantidadLicuadoPinaMas = (Button) findViewById(R.id.bt_licuadoPinaMas);
        Button bt_CantidadLicuadoPinaMenos = (Button) findViewById(R.id.bt_licuadoPinaMenos);
        Button bt_CantidadLicuadoBananoMas = (Button) findViewById(R.id.bt_licuadoBananoMas);
        Button bt_CantidadLicuadoBananoMenos = (Button) findViewById(R.id.bt_licuadoBananoMenos);
        Button bt_CantidadLicuadoFresaMas = (Button) findViewById(R.id.bt_licuadoFresaMas);
        Button bt_CantidadLicuadoFresaMenos = (Button) findViewById(R.id.bt_licuadoFresaMenos);
        //radio button para agua o leche
        Button bt_aguaLicuadoPapaya = (Button)findViewById(R.id.bt_aguaLicuadoPapaya);
        Button bt_lecheLicuadoPapaya = (Button)findViewById(R.id.bt_lecheLicuadoPapaya);
        Button bt_aguaLicuadoMelon = (Button)findViewById(R.id.bt_aguaLicuadoMelon);
        Button bt_lecheLicuadoMelon = (Button)findViewById(R.id.bt_lecheLicuadoMelon);
        Button bt_aguaLicuadoPina = (Button)findViewById(R.id.bt_aguaLicuadoPina);
        Button bt_lecheLicuadoPina = (Button)findViewById(R.id.bt_lecheLicuadoPina);
        Button bt_aguaLicuadoBanano = (Button)findViewById(R.id.bt_aguaLicuadoBanano);
        Button bt_lecheLicuadoBanano = (Button)findViewById(R.id.bt_lecheLicuadoBanano);
        Button bt_aguaLicuadoFresa = (Button)findViewById(R.id.bt_aguaLicuadoFresa);
        Button bt_lecheLicuadoFresa = (Button)findViewById(R.id.bt_lecheLicuadoFresa);
        Button bt_agregarOrdenLicuados = (Button)findViewById(R.id.bt_agregarOrdenLicuados);
        //
        // Registrar el onClickListener con la implementacion de arriba
        //
        bt_CantidadLicuadoPapayaMas.setOnClickListener(this);
        bt_CantidadLicuadoPapayaMenos.setOnClickListener(this);
        bt_CantidadLicuadoMelonMas.setOnClickListener(this);
        bt_CantidadLicuadoMelonMenos.setOnClickListener(this);
        bt_CantidadLicuadoPinaMas.setOnClickListener(this);
        bt_CantidadLicuadoPinaMenos.setOnClickListener(this);
        bt_CantidadLicuadoBananoMas.setOnClickListener(this);
        bt_CantidadLicuadoBananoMenos.setOnClickListener(this);
        bt_CantidadLicuadoFresaMas.setOnClickListener(this);
        bt_CantidadLicuadoFresaMenos.setOnClickListener(this);

        //radio buton para agua o leche
        bt_aguaLicuadoPapaya.setOnClickListener(this);
        bt_lecheLicuadoPapaya.setOnClickListener(this);
        bt_aguaLicuadoMelon.setOnClickListener(this);
        bt_lecheLicuadoMelon.setOnClickListener(this);
        bt_aguaLicuadoPina.setOnClickListener(this);
        bt_lecheLicuadoPina.setOnClickListener(this);
        bt_aguaLicuadoBanano.setOnClickListener(this);
        bt_lecheLicuadoBanano.setOnClickListener(this);
        bt_aguaLicuadoFresa.setOnClickListener(this);
        bt_lecheLicuadoFresa.setOnClickListener(this);
        bt_agregarOrdenLicuados.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId() /*to get clicked view id**/) {
            case R.id.bt_licuadoPapayaMas:
                CantidadLicuadoPapaya = sumar(CantidadLicuadoPapaya);
                txt_CantidadLicuadoPapaya.setText(CantidadLicuadoPapaya + "");
                ApplicationCafeteriaLorena.arrayLicuados[0][0] = CantidadLicuadoPapaya;
                break;
            case R.id.bt_licuadoPapayaMenos:
                CantidadLicuadoPapaya = restar(CantidadLicuadoPapaya);
                txt_CantidadLicuadoPapaya.setText(CantidadLicuadoPapaya + "");
                ApplicationCafeteriaLorena.arrayLicuados[0][0] = CantidadLicuadoPapaya;
                break;
            case R.id.bt_licuadoMelonMas:
                CantidadLicuadoMelon = sumar(CantidadLicuadoMelon);
                txt_CantidadLicuadoMelon.setText(CantidadLicuadoMelon + "");
                ApplicationCafeteriaLorena.arrayLicuados[1][0] = CantidadLicuadoMelon;
                break;
            case R.id.bt_licuadoMelonMenos:
                CantidadLicuadoMelon = restar(CantidadLicuadoMelon);
                txt_CantidadLicuadoMelon.setText(CantidadLicuadoMelon + "");
                ApplicationCafeteriaLorena.arrayLicuados[1][0] = CantidadLicuadoMelon;
                break;
            case R.id.bt_licuadoPinaMas:
                CantidadLicuadoPina = sumar(CantidadLicuadoPina);
                txt_CantidadLicuadoPina.setText(CantidadLicuadoPina + "");
                ApplicationCafeteriaLorena.arrayLicuados[2][0] = CantidadLicuadoPina;
                break;
            case R.id.bt_licuadoPinaMenos:
                CantidadLicuadoPina = restar(CantidadLicuadoPina);
                txt_CantidadLicuadoPina.setText(CantidadLicuadoPina + "");
                ApplicationCafeteriaLorena.arrayLicuados[2][0] = CantidadLicuadoPina;
                break;
            case R.id.bt_licuadoBananoMas:
                CantidadLicuadoBanano = sumar(CantidadLicuadoBanano);
                txt_CantidadLicuadoBanano.setText(CantidadLicuadoBanano + "");
                ApplicationCafeteriaLorena.arrayLicuados[3][0] = CantidadLicuadoBanano;
                break;
            case R.id.bt_licuadoBananoMenos:
                CantidadLicuadoBanano = restar(CantidadLicuadoBanano);
                txt_CantidadLicuadoBanano.setText(CantidadLicuadoBanano + "");
                ApplicationCafeteriaLorena.arrayLicuados[3][0] = CantidadLicuadoBanano;
                break;
            case R.id.bt_licuadoFresaMas:
                CantidadLicuadoFresa = sumar(CantidadLicuadoFresa);
                txt_CantidadLicuadoFresa.setText(CantidadLicuadoFresa + "");
                ApplicationCafeteriaLorena.arrayLicuados[4][0] = CantidadLicuadoFresa;
                break;
            case R.id.bt_licuadoFresaMenos:
                CantidadLicuadoFresa = restar(CantidadLicuadoFresa);
                txt_CantidadLicuadoFresa.setText(CantidadLicuadoFresa + "");
                ApplicationCafeteriaLorena.arrayLicuados[4][0] = CantidadLicuadoFresa;
                break;
            //radio button agua=0 o leche=1 o jugo de naranja=2
            case R.id.bt_aguaLicuadoPapaya:
                ApplicationCafeteriaLorena.arrayLicuados[0][1] = 0;
                break;
            case R.id.bt_lecheLicuadoPapaya:
                ApplicationCafeteriaLorena.arrayLicuados[0][1] = 1;
                break;
            case R.id.bt_aguaLicuadoMelon:
                ApplicationCafeteriaLorena.arrayLicuados[1][1] = 0;
                break;
            case R.id.bt_lecheLicuadoMelon:
                ApplicationCafeteriaLorena.arrayLicuados[1][1] = 1;
                break;
            case R.id.bt_aguaLicuadoPina:
                ApplicationCafeteriaLorena.arrayLicuados[2][1] = 0;
                break;
            case R.id.bt_lecheLicuadoPina:
                ApplicationCafeteriaLorena.arrayLicuados[2][1] = 1;
                break;
            case R.id.bt_aguaLicuadoBanano:
                ApplicationCafeteriaLorena.arrayLicuados[3][1] = 0;
                break;
            case R.id.bt_lecheLicuadoBanano:
                ApplicationCafeteriaLorena.arrayLicuados[3][1] = 1;
                break;
            case R.id.bt_aguaLicuadoFresa:
                ApplicationCafeteriaLorena.arrayLicuados[4][1] = 0;
                break;
            case R.id.bt_lecheLicuadoFresa:
                ApplicationCafeteriaLorena.arrayLicuados[4][1] = 1;
                break;
            case R.id.bt_agregarOrdenLicuados:
                Intent detalleActividad = new Intent(this, DetalleOrden.class);
                detalleActividad.putExtra("detalle",detalle);
                startActivity(detalleActividad);
                break;

        }
    }

        //
        /* Suma y resta cantidades**/
        //
        private int sumar(int vieneCantidad){
            if(vieneCantidad < 10)
                vieneCantidad++; // cantidad = cantidad + 1;
            else
                Toast.makeText(this, getString(R.string.str_msjCantidadMaxima), Toast.LENGTH_LONG).show();
            return (vieneCantidad);
        }

        private int restar(int vieneCantidad){
            if(vieneCantidad > 0)
                vieneCantidad--; // cantidad = cantidad - 1;
            else
                Toast.makeText(this, getString(R.string.str_msjCantidadCero), Toast.LENGTH_LONG).show();
            return (vieneCantidad);
        }

}
