package com.example.cafeterialorena.packageCafeteriaLorena;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.cafeterialorena.ApplicationCafeteriaLorena;
import com.example.cafeterialorena.R;

public class DatosEnvio extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_envio);

        int recibirDatosEnvio = getIntent().getExtras().getInt("datosEnvio");



    }

    public void enviarCorreo (View view){
        EditText nameEditText = (EditText)findViewById(R.id.ed_name);
        EditText addressEditText = (EditText)findViewById(R.id.ed_direccion);
        String name = nameEditText.getText().toString();
        String direccion = addressEditText.getText().toString();
        ApplicationCafeteriaLorena.cuerpo += "\n \n"+getString(R.string.str_deliveryInfo)+ "\n" + name + "\n"+direccion + "\n \n"+getString(R.string.str_granTotal)+" = " + ApplicationCafeteriaLorena.GranTotal;
        String subject = getString(R.string.str_tituloDetalleorden);
        Intent myIntent = new Intent(Intent.ACTION_SEND);
        myIntent.setType("*/*");
        myIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"euro2000@gmail.com"});
        myIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        myIntent.putExtra(Intent.EXTRA_TEXT, ApplicationCafeteriaLorena.cuerpo);
        if (myIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(myIntent);
        }

    }

    /*Below Method hide the keyboard when clicking somewhere else */
    public boolean dispatchTouchEvent(android.view.MotionEvent event) {
        if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                android.graphics.Rect outRect = new android.graphics.Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    android.view.inputmethod.InputMethodManager imm = (android.view.inputmethod.InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
}
