package com.example.cafeterialorena.packageCafeteriaLorena;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.cafeterialorena.ApplicationCafeteriaLorena;
import com.example.cafeterialorena.R;
import com.example.cafeterialorena.adapter.StableArrauAdapter;

import java.util.ArrayList;

public class DetalleOrden extends Activity  {

    final ArrayList<String> list = new ArrayList<String>();
    int datosEnvio = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_detalle_orden);
        setContentView(R.layout.item_orden);

        int recibirDetalle = getIntent().getExtras().getInt("detalle");
        ApplicationCafeteriaLorena.GranTotal = 0;
        final ListView listView = (ListView) findViewById(R.id.lv_detalleLista);
        String [] detalle = new String [30];
        String  item = "";


        //final ArrayList<String> list = new ArrayList<String>();

        //llenado detalle desayunos
        for (int i = 0; i < ApplicationCafeteriaLorena.arrayDesayunos.length; ++i) {
            item = "";
            switch (i) {
                case 0:  //huevos estrellados
                    if (ApplicationCafeteriaLorena.arrayDesayunos[i][0] > 0) {
                        if (ApplicationCafeteriaLorena.arrayDesayunos[i][1] == 0) {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosEstrellados) + ", " + getString(R.string.str_cafe) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        } else {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosEstrellados) + ", " + getString(R.string.str_jugo) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        }
                        list.add(item);
                    }
                    break;
                case 1:  //huevos revueltos
                    if (ApplicationCafeteriaLorena.arrayDesayunos[i][0] > 0) {
                        if (ApplicationCafeteriaLorena.arrayDesayunos[i][1] == 0) {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosRevueltos) + ", " + getString(R.string.str_cafe) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        } else {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosRevueltos) + ", " + getString(R.string.str_jugo) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        }
                        list.add(item);
                    }
                    break;
                case 2:  //huevos con tomate y cebolla
                    if (ApplicationCafeteriaLorena.arrayDesayunos[i][0] > 0) {
                        if (ApplicationCafeteriaLorena.arrayDesayunos[i][1] == 0) {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosRevueltosTomateCebolla) + ", " + getString(R.string.str_cafe) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        } else {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosRevueltosTomateCebolla) + ", " + getString(R.string.str_jugo) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        }
                        list.add(item);
                    }
                    break;
                case 3:  //huevos con queso pita
                    if (ApplicationCafeteriaLorena.arrayDesayunos[i][0] > 0) {
                        if (ApplicationCafeteriaLorena.arrayDesayunos[i][1] == 0) {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosQuesoPita) + ", " + getString(R.string.str_cafe) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        } else {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosQuesoPita) + ", " + getString(R.string.str_jugo) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        }
                        list.add(item);
                    }
                    break;
                case 4:  //huevos revueltos con Salchicha
                    if (ApplicationCafeteriaLorena.arrayDesayunos[i][0] > 0) {
                        if (ApplicationCafeteriaLorena.arrayDesayunos[i][1] == 0) {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosSalchica) + ", " + getString(R.string.str_cafe) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        } else {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosSalchica) + ", " + getString(R.string.str_jugo) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        }
                        list.add(item);
                    }
                    break;
                case 5:  //huevos con tomate y cebolla
                    if (ApplicationCafeteriaLorena.arrayDesayunos[i][0] > 0) {
                        if (ApplicationCafeteriaLorena.arrayDesayunos[i][1] == 0) {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosJamon) + ", " + getString(R.string.str_cafe) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        } else {
                            item += (ApplicationCafeteriaLorena.arrayDesayunos[i][0] + " " + getString(R.string.str_huevosJamon) + ", " + getString(R.string.str_jugo) + " $" + subtotal(ApplicationCafeteriaLorena.arrayDesayunos[i][0], 18));
                        }
                        list.add(item);
                    }
                    break;
            }
        }

        //llenado detalle para panes
        for (int i = 0; i < ApplicationCafeteriaLorena.arrayPanes.length; ++i) {
            item = "";
            switch (i) {
                case 0:  //pan con carne
                    if (ApplicationCafeteriaLorena.arrayPanes[i][1] > 0) {
                        item += (ApplicationCafeteriaLorena.arrayPanes[i][1] + " " + getString(R.string.str_panCarne) + " $" + subtotal(ApplicationCafeteriaLorena.arrayPanes[i][1], ApplicationCafeteriaLorena.arrayPanes[i][0]));
                        list.add(item);
                    }
                    break;
                case 1:  //pan con pollo
                    if (ApplicationCafeteriaLorena.arrayPanes[i][1] > 0) {
                        item += (ApplicationCafeteriaLorena.arrayPanes[i][1] + " " + getString(R.string.str_panChicken) +  " $" + subtotal(ApplicationCafeteriaLorena.arrayPanes[i][1], ApplicationCafeteriaLorena.arrayPanes[i][0]));
                        list.add(item);
                    }
                    break;
                case 2:  //pan con huevo jamon y queso kraft
                    if (ApplicationCafeteriaLorena.arrayPanes[i][1] > 0) {
                        item += (ApplicationCafeteriaLorena.arrayPanes[i][1] + " " + getString(R.string.str_panJamonQueso) + " $" + subtotal(ApplicationCafeteriaLorena.arrayPanes[i][1], ApplicationCafeteriaLorena.arrayPanes[i][0]));
                        list.add(item);
                    }
                    break;
                case 3:  //pan con huevo chorizo y pita
                    if (ApplicationCafeteriaLorena.arrayPanes[i][1] > 0) {
                        item += (ApplicationCafeteriaLorena.arrayPanes[i][1] + " " + getString(R.string.str_panChorizoPita) + " $" + subtotal(ApplicationCafeteriaLorena.arrayPanes[i][1], ApplicationCafeteriaLorena.arrayPanes[i][0]));
                        list.add(item);
                    }
                    break;
                case 4:  //pan con jamon y queso kraft
                    if (ApplicationCafeteriaLorena.arrayPanes[i][1] > 0) {
                        item += (ApplicationCafeteriaLorena.arrayPanes[i][1] + " " + getString(R.string.str_panJamonQueso) + " $" + subtotal(ApplicationCafeteriaLorena.arrayPanes[i][1], ApplicationCafeteriaLorena.arrayPanes[i][0]));
                        list.add(item);
                    }
                    break;
                case 5:  //pan con huevo revuelto tomate y cebolla
                    if (ApplicationCafeteriaLorena.arrayPanes[i][1] > 0) {
                        item += (ApplicationCafeteriaLorena.arrayPanes[i][1] + " " + getString(R.string.str_panTomateCebolla) + " $" + subtotal(ApplicationCafeteriaLorena.arrayPanes[i][1], ApplicationCafeteriaLorena.arrayPanes[i][0]));
                        list.add(item);
                    }
                    break;
                case 6:  //pan con huevo revuelto con salchicha
                    if (ApplicationCafeteriaLorena.arrayPanes[i][1] > 0) {
                        item += (ApplicationCafeteriaLorena.arrayPanes[i][1] + " " + getString(R.string.str_panHuevoSalchicha) + " $" + subtotal(ApplicationCafeteriaLorena.arrayPanes[i][1], ApplicationCafeteriaLorena.arrayPanes[i][0]));
                        list.add(item);
                    }
                    break;
            }
        }

        //llenado detalle licuados
        for (int i = 0; i < ApplicationCafeteriaLorena.arrayLicuados.length; ++i) {
            item = "";
            switch (i) {
                case 0:  //licuado de papaya
                    if (ApplicationCafeteriaLorena.arrayLicuados[i][0] > 0) {
                        if (ApplicationCafeteriaLorena.arrayLicuados[i][1] == 0) {
                            item += (ApplicationCafeteriaLorena.arrayLicuados[i][0] + " " + getString(R.string.str_licuadoPapaya) + ", " + getString(R.string.str_licuadoAgua) + " $" + subtotal(ApplicationCafeteriaLorena.arrayLicuados[i][0], 10));
                        } else {
                            item += (ApplicationCafeteriaLorena.arrayLicuados[i][0] + " " + getString(R.string.str_licuadoPapaya) + ", " + getString(R.string.str_licuadoLeche) + " $" + subtotal(ApplicationCafeteriaLorena.arrayLicuados[i][0], 10));
                        }
                        list.add(item);
                    }
                    break;
                case 1:  //licuado de melon
                    if (ApplicationCafeteriaLorena.arrayLicuados[i][0] > 0) {
                        if (ApplicationCafeteriaLorena.arrayLicuados[i][1] == 0) {
                            item += (ApplicationCafeteriaLorena.arrayLicuados[i][0] + " " + getString(R.string.str_licuadoMelon) + ", " + getString(R.string.str_licuadoAgua) + " $" + subtotal(ApplicationCafeteriaLorena.arrayLicuados[i][0], 10));
                        } else {
                            item += (ApplicationCafeteriaLorena.arrayLicuados[i][0] + " " + getString(R.string.str_licuadoMelon) + ", " + getString(R.string.str_licuadoLeche) + " $" + subtotal(ApplicationCafeteriaLorena.arrayLicuados[i][0], 10));
                        }
                        list.add(item);
                    }
                    break;
                case 2:  //licuado de pina
                    if (ApplicationCafeteriaLorena.arrayLicuados[i][0] > 0) {
                        if (ApplicationCafeteriaLorena.arrayLicuados[i][1] == 0) {
                            item += (ApplicationCafeteriaLorena.arrayLicuados[i][0] + " " + getString(R.string.str_licuadoPina) + ", " + getString(R.string.str_licuadoAgua) + " $" + subtotal(ApplicationCafeteriaLorena.arrayLicuados[i][0], 10));
                        } else {
                            item += (ApplicationCafeteriaLorena.arrayLicuados[i][0] + " " + getString(R.string.str_licuadoPina) + ", " + getString(R.string.str_licuadoLeche) + " $" + subtotal(ApplicationCafeteriaLorena.arrayLicuados[i][0], 10));
                        }
                        list.add(item);
                    }
                    break;
                case 3:  //licuado de banano
                    if (ApplicationCafeteriaLorena.arrayLicuados[i][0] > 0) {
                        if (ApplicationCafeteriaLorena.arrayLicuados[i][1] == 0) {
                            item += (ApplicationCafeteriaLorena.arrayLicuados[i][0] + " " + getString(R.string.str_licuadoBanano) + ", " + getString(R.string.str_licuadoAgua) + " $" + subtotal(ApplicationCafeteriaLorena.arrayLicuados[i][0], 10));
                        } else {
                            item += (ApplicationCafeteriaLorena.arrayLicuados[i][0] + " " + getString(R.string.str_licuadoBanano) + ", " + getString(R.string.str_licuadoLeche) + " $" + subtotal(ApplicationCafeteriaLorena.arrayLicuados[i][0], 10));
                        }
                        list.add(item);
                    }
                    break;
                case 4:  //licuado de fresa
                    if (ApplicationCafeteriaLorena.arrayLicuados[i][0] > 0) {
                        if (ApplicationCafeteriaLorena.arrayLicuados[i][1] == 0) {
                            item += (ApplicationCafeteriaLorena.arrayLicuados[i][0] + " " + getString(R.string.str_licuadoFresa) + ", " + getString(R.string.str_licuadoAgua) + " $" + subtotal(ApplicationCafeteriaLorena.arrayLicuados[i][0], 10));
                        } else {
                            item += (ApplicationCafeteriaLorena.arrayLicuados[i][0] + " " + getString(R.string.str_licuadoFresa) + ", " + getString(R.string.str_licuadoLeche) + " $" + subtotal(ApplicationCafeteriaLorena.arrayLicuados[i][0], 10));
                        }
                        list.add(item);
                    }
                    break;
            }
        }

           final StableArrauAdapter Adapter = new StableArrauAdapter(this, android.R.layout.simple_list_item_1, list);
           listView.setAdapter(Adapter);



            /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


                @Override
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                    final String item = (String) parent.getItemAtPosition(position);
                    view.animate().setDuration(2000).alpha(0).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            list.remove(item);
                            Adapter.notifyDataSetChanged();
                            view.setAlpha(1);
                        }
                    });

                }

            }); */



    }

    public int subtotal (int cantidad, int precio){
        int subtotal = cantidad * precio;
        ApplicationCafeteriaLorena.GranTotal += subtotal;
        return subtotal;

    }

    public void ponerOrden(View view){

        for (int i =0; i<list.size();i++){
            ApplicationCafeteriaLorena.cuerpo += list.get(i) + "\n";

        Intent datosEnvioActividad = new Intent(this, DatosEnvio.class);
        datosEnvioActividad.putExtra("datosEnvio",datosEnvio);
        startActivity(datosEnvioActividad);

        }


    }

    public void agregarMas(View view){
        startActivity(new Intent(this,MainActivity.class));
    }
}
