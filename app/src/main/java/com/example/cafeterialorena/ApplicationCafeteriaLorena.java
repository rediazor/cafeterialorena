package com.example.cafeterialorena;

import android.app.Application;

import java.util.ArrayList;

public class ApplicationCafeteriaLorena extends Application {

    //
    //array de panes
    // son 6 tipos de desayunos
    //en el array se guarda la cantidad de cada tipo de desayuno y si quieren cafe o jugo
    //cafe = 0 y jugo = 1
    public static int[][] arrayDesayunos;
    static {
        arrayDesayunos = new int[6][2];
    }


    //
    //array de panes
    // son 6 tipos de panes
    //en el array se guardaran los precios unitarios de cada pan y
    //la cantidad pedida de cada tipo de pan
    public static int[][] arrayPanes;
    static {
        arrayPanes = new int[7][2];
    }

    //
    //array de licuados de tipo entero
    //son 5 tipos de licuado
    //se guardara la cantidad del licuados y la base del licuado 0=agua 1=leche y 2=jugo de naranja
    //
    public static int [][] arrayLicuados;
    static {
        arrayLicuados = new int[5][2];
    }

    public static String nombre = "";
    public static String direccion = "";
    public static String cuerpo = "";
    public static int GranTotal = 0;

   // public static final ArrayList<String> list = new ArrayList<String>();
}
